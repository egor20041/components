package main

import (
	"bufio"
	"database/sql"
	"errors"
	"os"
	"strings"
)

type ChangesetMeta struct {
	Type    string
	ID      string
	Author  string
	Date    string
	Comment string
}

type Changeset struct {
	ChangesetMeta
	Body string
}

type AutoChangeProcessor struct {
	db       *sql.DB
	changes []*Changeset
}

func GetChanges(filename string) ([]*Changeset, error) {
	p, err := NewAutoChangeProcessor(filename)
	if err != nil {
		return nil, err
	}
	return p.Get(), nil
}

func NewAutoChangeProcessor(filename string) (AutoChangeProcessor, error) {
	p := AutoChangeProcessor{}
	err := p.load(filename)
	return p, err
}

func (p *AutoChangeProcessor) Get() []*Changeset {
	return p.changes
}

func (p *AutoChangeProcessor) load(filename string) error {
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	var line, meta, change string
	var set []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line = scanner.Text()
		p.processLine(line, &meta, &set, &change)
	}
	p.processLine("\xFF", &meta, &set, &change)

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}

func (p *AutoChangeProcessor) processLine(line string, metaStr *string, set *[]string, change *string) error {
	line = strings.TrimSpace(line)
	if len(line) == 0 || strings.HasPrefix(line, "--") {
		return nil
	}
	if line[0] != '#' && line[0] != '\xFF' {
		*set = append(*set, line)
		return nil
	}
	if len(*set) > 0 {
		*change = strings.Join(*set, "\n")
		err, meta := p.parseMeta(*metaStr)
		if err != nil {
			return err
		}
		if err := p.validateMeta(meta); err != nil {
			return err
		}
		p.changes = append(p.changes, &Changeset{
			ChangesetMeta{
				meta.Type,
				meta.ID,
				meta.Author,
				meta.Date,
				meta.Comment,
			},
			*change,
		})
		*set = nil
	}
	*metaStr = line
	return nil
}

func (p *AutoChangeProcessor) parseMeta(metaStr string) (error, ChangesetMeta) {
	meta := ChangesetMeta{}
	metaParams := strings.SplitN(metaStr[1:], "|", 5)
	if len(metaParams) < 5 {
		return errors.New("meta header is invalid"), meta
	}
	for i := range metaParams {
		metaParams[i] = strings.TrimSpace(metaParams[i])
	}
	meta.Type = metaParams[0]
	meta.ID = metaParams[1]
	meta.Author = metaParams[2]
	meta.Date = metaParams[3]
	meta.Comment = metaParams[4]
	return nil, meta
}

func (p *AutoChangeProcessor) validateMeta(meta ChangesetMeta) error {
	if len(meta.ID) > 36 {
		return errors.New("meta validation: id should be less than or equal to 36 chars")
	}
	if len(meta.Author) > 36 {
		return errors.New("meta validation: author should be less than or equal to 36 chars")
	}
	if len(meta.Comment) > 500 {
		return errors.New("meta validation: comment should be less than or equal to 500 chars")
	}
	return nil
}
