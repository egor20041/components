package main

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"
)

type Service interface {
	Apply(*Changeset) error
}

type MysqlService struct {
	db *sql.DB
}

func (s MysqlService) Apply(cs *Changeset) error {
	tx, err := s.db.Begin()
	if err != nil {
		return err
	}
	body := strings.Trim(strings.TrimSpace(cs.Body), ";")
	queries := strings.Split(body, ";\n")
	for _, query := range queries {
		_, err = tx.Exec(query)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	return nil
}

type ServiceManager struct {
	config    map[string]ServiceConfig
	instances map[string]Service
}

func NewServiceManager(config map[string]ServiceConfig) ServiceManager {
	return ServiceManager{
		config,
		make(map[string]Service),
	}
}

func (s *ServiceManager) GetService(name string) (Service, error) {
	if si, ok := s.instances[name]; ok {
		return si, nil
	}
	c, ok := s.config[name]
	if !ok {
		return nil, errors.New("unknown service: " + name)
	}
	switch c.Type {
	case "mysql":
		c, err := getMysqlConn(c.Params)
		if err != nil {
			return nil, err
		}
		si := MysqlService{c}
		s.instances[name] = si
		return si, nil
	default:
		return nil, errors.New("unknown service type: " + c.Type)
	}
}

func getMysqlConn(params map[string]interface{}) (*sql.DB, error) {
	dbHost, ok := params["host"]
	if !ok {
		dbHost = "localhost"
	}
	dbPort, ok := params["port"]
	if !ok {
		dbPort = "3306"
	}
	dbUsername, ok := params["username"]
	if !ok {
		dbUsername = "root"
	}
	dbPassword, _ := params["password"]
	dbName, _ := params["dbName"]
	cs := fmt.Sprintf("%s:%s@(%s:%s)/%s", dbUsername, dbPassword, dbHost, dbPort, dbName)
	return sql.Open("mysql", cs)
}

type AutoChangeLogService struct {
	db      *sql.DB
	applied []string
}

func NewAutoChangeLogService(c ServiceConfig) (*AutoChangeLogService, error) {
	if c.Type != "mysql" {
		return nil, errors.New("now ac_log supports only mysql")
	}
	db, err := getMysqlConn(c.Params)
	if err != nil {
		return nil, err
	}
	s := &AutoChangeLogService{
		db,
		make([]string, 0),
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	err = s.loadAppliedIds()
	if err != nil {
		return nil, err
	}
	return s, nil
}

func (s *AutoChangeLogService) loadAppliedIds() error {
	r, err := s.db.Query("SELECT `ac_id` FROM `ac_log`")
	if err != nil {
		return err
	}
	defer r.Close()

	ids := make([]string, 0)
	for r.Next() {
		var id string
		err := r.Scan(&id)
		if err != nil {
			return err
		}

		ids = append(ids, id)
	}
	s.applied = ids

	return nil
}

func (s *AutoChangeLogService) IsApplied(id string) bool {
	for _, v := range s.applied {
		if v == id {
			return true
		}
	}
	return false
}

func (s *AutoChangeLogService) Add(cs *Changeset) error {
	s.applied = append(s.applied, cs.ID)
	_, err := s.db.Exec("INSERT INTO `ac_log` (`ac_id`, `ac_author`, `ac_date`, `ac_comment`)"+
		"VALUES (?, ?, ?, ?)", cs.ID, cs.Author, cs.Date, cs.Comment)
	if err != nil {
		return err
	}
	return nil
}
