Migration is a tool to update version of project's databases and other stuff.

To make project migrations you need to create a directory with the following
contents:
- autochange.txt
- config.json
- optionally subdirectories for executables

Usage:
```
> migration /path/to/migrations

or using working directory:
> cd /path/to/migrations
> migrate
```

### autochange.txt

Change sets is defined by header string that begins with `#` char and have the
following format:
```
#<service name> | <id> | <author> | <date time> | <comment>
```
For clarity it also can be divided with a line of dashes. After the header
follows the changeset body if necessary. `<service name>` specifies what kind
of change that is and it configured in `config.json`. `id` is an unique string
that identifies changeset and also defines the filename for changes that applied
with external files. `author` is VCS username, `date time` is a date/time of the
change (recommended to set it as close as possible to commit time). `comment` is
one-line text to explain sense of the changeset.

Example:
```
#MySQL | 64643d97-015f-44ca-a5d8-8b729dda7f5b | andrey | 07-08-2013 15:13:34 | comment text

INSERT INTO `users` VALUES(1, 'root', 100500);

--------------------------------------------------------------------------------
#PHP | pkvY3f | andrey | 07-08-2013 13:40:23 | comment text
```

### config.json

There's two keys in the root: `acLog` to config the db when autochange log is
stored and `services` to config the services that need to be updated with the
migrations. `services` is a collection when the key is the name of service that
may be any string and it will be matched to service name in `autochange.txt`.
Both autochange log and service entries has `type` to specify the driver and
`params` to pass it to the driver.

Example:
```json
{
  "acLog": {
    "type": "mysql",
    "params": {
      "host": "",
      "port": "",
      "username": "",
      "password": "",
      "dbName": ""
    }
  },
  "services": {
    "MySql": {
      "type": "mysql",
      "params": {
        "host": "",
        "port": "",
        "username": "",
        "password": "",
        "dbName": ""
      }
    },
    "MySql2": {
      "type": "mysql",
      "params": {
        "host": "",
        "port": "",
        "username": "",
        "password": "",
        "dbName": ""
      }
    },
    "Elastic": {
      "type": "elasticsearch",
      "params": {
        "host": "",
        "port": ""
      }
    },
    "Exec": {
      "type": "executable",
      "params": {
        "dir": "./exec"
      }
    }
  }
}
```
