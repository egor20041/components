package main

import (
	"bufio"
	"encoding/json"
	"os"
)

type ServiceConfig struct {
	Type   string
	Params map[string]interface{}
}

type Config struct {
	ACLog    ServiceConfig
	Services map[string]ServiceConfig
}

func GetConfig(filename string) (*Config, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	config := &Config{}
	reader := bufio.NewReader(f)
	decoder := json.NewDecoder(reader)
	if err := decoder.Decode(config); err != nil {
		return nil, err
	}

	return config, nil
}
