package main

import (
	"errors"
	"fmt"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

const (
	ChangesFile = "autochange.txt"
	ConfigFile  = "config.json"
)

func main() {
	dir, err := getInputDir()
	if err != nil {
		log.Fatal(err)
	}

	config, err := GetConfig(dir + "/" + ConfigFile)
	if err != nil {
		log.Fatal(err)
	}

	changes, err := GetChanges(dir + "/" + ChangesFile)
	if err != nil {
		log.Fatal(err)
	}

	sm := NewServiceManager(config.Services)

	acLogService, err := NewAutoChangeLogService(config.ACLog)
	if err != nil {
		log.Fatal(err)
	}

	for _, cs := range changes {
		if acLogService.IsApplied(cs.ID) {
			continue
		}

		s, err := sm.GetService(cs.Type)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("Apply %s %s... ", cs.Type, cs.ID)
		err = s.Apply(cs)
		if err != nil {
			fmt.Println("Failed")
			log.Fatal(err)
		}
		fmt.Println("OK")

		err = acLogService.Add(cs)
		if err != nil {
			log.Fatal(err)
		}
	}
}

// Returns user-specified directory or current working directory if it not specified explicitly
func getInputDir() (string, error) {
	if len(os.Args) < 2 {
		dir, err := os.Getwd()
		if err != nil {
			return "", errors.New("cannot get working directory")
		}
		return dir, nil
	}

	var dir string = os.Args[len(os.Args)-1]
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		return "", errors.New("specified dir doesn't exists")
	}

	return dir, nil
}
